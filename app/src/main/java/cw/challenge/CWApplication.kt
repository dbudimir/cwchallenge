package cw.challenge

import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.support.v4.app.Fragment
import cw.challenge.di.component.AppComponent
import timber.log.Timber

/**
 * Created by Dario Budimir on 08/03/18.
 */

class CWApplication: Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) setupDebugRuntime()

        appComponent = AppComponent.create(this, BuildConfig.BASE_URL)
        appComponent.inject(this)
    }

    private fun setupDebugRuntime() {
        Timber.plant(Timber.DebugTree())
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .build())

        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build())
    }
}