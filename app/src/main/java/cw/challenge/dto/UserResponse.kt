package cw.challenge.dto

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import cw.challenge.db.Converters

@Entity(tableName = "UserResponse")
@TypeConverters(Converters::class)
data class UserResponse (
        val skills: List<String> = emptyList(),
        var ranks: Ranks,
        val honor: Int,
        var codeChallenges: CodeChallenges?,
        val name: String = "",
        val clan: String = "",
        val leaderboardPosition: Int,
        @PrimaryKey val username: String
)