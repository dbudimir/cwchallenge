package cw.challenge.dto

data class DataItem(
	val languages: List<String?>? = null,
	val completedLanguages: List<String?>? = null,
	val rankName: String? = null,
	val name: String? = null,
	val description: String? = null,
	val rank: Int? = null,
	val id: String? = null,
	val tags: List<String?>? = null,
	val completedAt: String? = null
)
