package cw.challenge.dto

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Overall(
		var score: Int = -1,
		var color: String = "",
		var name: String = "",
		var rank: Int = -1
)