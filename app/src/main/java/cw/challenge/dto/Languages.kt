package cw.challenge.dto

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Languages(
		var haskell: Haskell? = null,
		var python: Python? = null,
		var cpp: Cpp? = null,
		var coffeescript: Coffeescript? = null,
		var C: C? = null,
		var elixir: Elixir? = null,
		var bf: Bf? = null,
		var clojure: Clojure? = null,
		var ocaml: Ocaml? = null,
		var kotlin: Kotlin? = null,
		var go: Go? = null,
		var javascript: Javascript? = null,
		var ruby: Ruby? = null,
		var sql: Sql? = null,
		var rust: Rust? = null,
		var csharp: Csharp? = null,
		var R: R? = null,
		var java: Java? = null,
		var shell: Shell? = null,
		var fsharp: Fsharp? = null,
		var php: Php? = null,
		var lua: Lua? = null,
		var typescript: Typescript? = null,
		var solidity: Solidity? = null
)
