package cw.challenge.dto

import android.arch.persistence.room.Entity

@Entity
data class CodeChallenges(
		var totalAuthored: Int = -1,
		var totalCompleted: Int = -1
)
