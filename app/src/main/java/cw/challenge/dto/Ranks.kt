package cw.challenge.dto

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Ranks (
		var languages: Languages? = null,
		var overall: Overall? = null
)
