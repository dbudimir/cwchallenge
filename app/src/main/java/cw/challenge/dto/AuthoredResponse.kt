package cw.challenge.dto

data class AuthoredResponse(
	val data: List<DataItem?>? = null
)
