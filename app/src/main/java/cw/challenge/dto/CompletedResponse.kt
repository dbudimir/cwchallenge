package cw.challenge.dto

data class CompletedResponse(
	val totalItems: Int = -1,
	val data: List<DataItem?>? = null,
	val totalPages: Int = -1
)
