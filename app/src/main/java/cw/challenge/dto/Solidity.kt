package cw.challenge.dto

import android.arch.persistence.room.Entity

@Entity
data class Solidity(
		var score: Int? = -1,
		var color: String? = "",
		var name: String? = "",
		var rank: Int? = -1
)