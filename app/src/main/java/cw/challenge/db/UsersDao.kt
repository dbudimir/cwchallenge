package cw.challenge.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import cw.challenge.dto.UserResponse

/**
 * Created by Dario Budimir on 10/03/18.
 */

@Dao
interface UsersDao {

    @Query("SELECT * FROM UserResponse ORDER BY ROWID DESC LIMIT 5")
    fun getUsers(): LiveData<List<UserResponse>>

    @Query("SELECT * FROM UserResponse WHERE username = :username")
    fun loadUser(username: String?): LiveData<List<UserResponse>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(person: UserResponse?)

    @Delete
    fun delete(peopleModel: UserResponse)

    @Query("SELECT COUNT(*) from UserResponse")
    fun countUsers(): Int
}