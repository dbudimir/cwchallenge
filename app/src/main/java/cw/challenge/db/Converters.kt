package cw.challenge.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import cw.challenge.dto.CodeChallenges
import cw.challenge.dto.Ranks


/**
 * Created by Dario Budimir on 10/03/18.
 */

class Converters {
    @TypeConverter
    fun fromStringList(listValue: String): List<String> {
        val listType = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(listValue, listType)
    }

    @TypeConverter
    fun fromList(list: List<String>?): String {
        val gson = Gson()
        return gson.toJson(list).toString()
    }

    @TypeConverter
    fun fromStringRanks(ranks: String): Ranks {
        val listType = object : TypeToken<Ranks>() {}.type
        return Gson().fromJson(ranks, listType)
    }

    @TypeConverter
    fun fromRanks(ranks: Ranks): String {
        val gson = Gson()
        return gson.toJson(ranks).toString()
    }

    @TypeConverter
    fun fromStringCodeChallenges(codeChallenges: String): CodeChallenges {
        val codeChallengesType = object : TypeToken<CodeChallenges>() {}.type
        return Gson().fromJson(codeChallenges, codeChallengesType)
    }

    @TypeConverter
    fun fromCodeChallenges(codeChallenges: CodeChallenges?): String {
        val gson = Gson()
        return gson.toJson(codeChallenges).toString()
    }
}