package cw.challenge.db

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import cw.challenge.dto.UserResponse


/**
 * Created by Dario Budimir on 10/03/18.
 */

@Database(entities = [(UserResponse::class)], version = 1)
abstract class UsersDatabase : RoomDatabase() {
    abstract fun usersDao(): UsersDao
}