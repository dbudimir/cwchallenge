package cw.challenge.ui

import android.os.Bundle
import android.view.Menu
import cw.challenge.R
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.SearchView
import cw.challenge.CWBus
import cw.challenge.ui.base.BaseActivity
import cw.challenge.utils.component


class MainActivity : BaseActivity() {

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            component.navigationController.navigateToSearch(this)
        }
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val searchView: SearchView?
        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView

        searchView.setOnQueryTextListener(onQueryTextListener)
        searchView.setQuery("Voile", false)
        return super.onCreateOptionsMenu(menu)
    }

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            CWBus.publish(query)
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            return true
        }
    }
}
