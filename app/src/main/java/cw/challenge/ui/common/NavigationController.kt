package cw.challenge.ui.common

import android.support.v4.app.FragmentActivity
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import cw.challenge.R
import cw.challenge.ui.common.search.SearchFragment
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 09/03/18.
 */

@Singleton
class NavigationController @Inject constructor() {

    fun navigateToSearch(activity: FragmentActivity) {
        val searchFragment = SearchFragment()
        activity.supportFragmentManager.beginTransaction()
                .replace(R.id.container, searchFragment)
                .commitAllowingStateLoss()
    }

    fun navigateToFragment(activity: FragmentActivity, fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commitAllowingStateLoss()
    }

    fun showError(activity: FragmentActivity, error: String?) {
        Snackbar.make(activity.findViewById(android.R.id.content), error ?: "Error", Snackbar.LENGTH_SHORT).show()
    }
}
