package cw.challenge.ui.details.completed


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import cw.challenge.R
import cw.challenge.dto.DataItem
import cw.challenge.ui.base.BaseFragment
import cw.challenge.utils.component
import cw.challenge.viewmodel.CompletedViewModel
import kotlinx.android.synthetic.main.fragment_completed.*

class CompletedFragment : BaseFragment(), CompletedAdapter.OnItemClickListener {

    private lateinit var completedViewModel: CompletedViewModel
    private lateinit var completedAdapter: CompletedAdapter
    private var completedList: List<DataItem> = emptyList()
    private lateinit var userID: String
    private var maxPageNo = 0
    private var pageNo = 0

    companion object {
        private val ARG = "user_id"
        fun newInstance(userid: String): CompletedFragment {
            val args: Bundle = Bundle()
            args.putSerializable(ARG, userid)
            val fragment = CompletedFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayout(): Int {
        return R.layout.fragment_completed
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userID = arguments?.getString(ARG).toString()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        completedViewModel = CompletedViewModel.create(this)
        component.inject(completedViewModel)
        progress_bar.visibility = View.VISIBLE
        completedViewModel.search(userID, pageNo)
        completedViewModel.completedResult.observe(this, Observer {
            progress_bar.visibility = View.INVISIBLE
            if (completedList.size > 0) {
                completedAdapter.addList(it?.data?.data as List<DataItem>)
            } else {
                maxPageNo = it?.data?.totalPages ?: 0
                completedList = it?.data?.data as List<DataItem>
                completedAdapter = CompletedAdapter(completedList, this)
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = completedAdapter
            }
        })

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val visibleThreshold = 25

                var loading = true
                var previousTotal = 0
                with(recyclerView?.layoutManager as LinearLayoutManager){

                    val visibleItemCount = childCount
                    val totalItemCount = itemCount
                    val firstVisibleItem = findFirstVisibleItemPosition()

                    if (loading && totalItemCount > previousTotal){
                        loading = false
                        progress_bar.visibility = View.INVISIBLE
                        previousTotal = totalItemCount
                    }

                    if(!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)){
                        if (maxPageNo == pageNo -1) {
                            Snackbar.make(recyclerView, R.string.noMoreData, Snackbar.LENGTH_LONG).show()
                        } else {
                            progress_bar.visibility = View.VISIBLE
                            pageNo++
                            loading = true
                            completedViewModel.search(userID, pageNo)
                        }
                    }
                }
            }
        })
    }

    override fun onItemClick(item: DataItem) {
        val simpleAlert = AlertDialog.Builder(requireContext()).create()
        simpleAlert.setTitle(item.name)
        simpleAlert.setMessage("Languages: ${item.completedLanguages.toString()}")

        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->
            dialogInterface.dismiss()
        })
        simpleAlert.show()
    }
}
