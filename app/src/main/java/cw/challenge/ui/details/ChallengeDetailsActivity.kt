package cw.challenge.ui.details

import android.os.Bundle
import cw.challenge.R
import cw.challenge.ui.base.BaseActivity

class ChallengeDetailsActivity : BaseActivity() {

    override fun getLayout(): Int {
        return R.layout.activity_challenge_details
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
