package cw.challenge.ui.details.authored

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import cw.challenge.R
import cw.challenge.dto.DataItem

/**
 * Created by Dario Budimir on 11/03/18.
 */

class AuthoredAdapter(authoredList: List<DataItem>, listener: OnItemClickListener) : RecyclerView.Adapter<AuthoredAdapter.RecyclerViewHolder>() {

    private var listAuthored: List<DataItem> = authoredList
    private var listenerAuthored: OnItemClickListener = listener

    interface OnItemClickListener {
        fun onItemClick(contact: DataItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.authored_item_list, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val currentItem: DataItem = listAuthored[position]

        val itemName = currentItem.name
        val itemRankName = currentItem.rankName

        holder.name.text = itemName
        holder.rankName.text = itemRankName

        holder.bind(currentItem, listenerAuthored)
    }

    override fun getItemCount(): Int {
        return listAuthored.size
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name = itemView.findViewById<TextView>(R.id.name)
        var rankName = itemView.findViewById<TextView>(R.id.rankName)

        fun bind(contact: DataItem, listener: OnItemClickListener) {
            itemView.setOnClickListener {
                listener.onItemClick(contact)
            }
        }
    }
}