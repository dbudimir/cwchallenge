package cw.challenge.ui.details.completed

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import cw.challenge.R
import cw.challenge.dto.DataItem
import timber.log.Timber

/**
 * Created by Dario Budimir on 11/03/18.
 */

class CompletedAdapter(contacts: List<DataItem>, listener: OnItemClickListener) : RecyclerView.Adapter<CompletedAdapter.RecyclerViewHolder>() {

    private var listContacts: List<DataItem> = contacts
    private var listenerContact: OnItemClickListener = listener

    interface OnItemClickListener {
        fun onItemClick(item: DataItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.completed_item_list, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val currentItem: DataItem = listContacts[position]

        val itemName = currentItem.name
        val itemCompletedAt = currentItem.completedAt

        holder.name.text = itemName
        holder.completedAt.text = itemCompletedAt

        holder.bind(currentItem, listenerContact)
    }

    override fun getItemCount(): Int {
        return listContacts.size
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name = itemView.findViewById<TextView>(R.id.name)
        var completedAt = itemView.findViewById<TextView>(R.id.completedAt)

        fun bind(contact: DataItem, listener: OnItemClickListener) {
            itemView.setOnClickListener {
                listener.onItemClick(contact)
            }
        }
    }

    fun addList(itemList: List<DataItem>) {
        listContacts = listContacts.plus(itemList)
        notifyDataSetChanged()
        Timber.d("TOTAL: ${listContacts.size}")
    }
}