package cw.challenge.ui.details

import android.content.Context
import android.os.Bundle
import cw.challenge.R
import cw.challenge.ui.base.BaseActivity
import android.content.Intent
import android.support.v4.app.Fragment
import cw.challenge.ui.details.authored.AuthoredFragment
import cw.challenge.ui.details.completed.CompletedFragment
import cw.challenge.utils.component
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : BaseActivity() {

    private lateinit var userid: String

    companion object {
        private val INTENT_USER_ID = "user_id"
        fun newIntent(context: Context?, user: String): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(INTENT_USER_ID, user)
            return intent
        }
    }

    override fun getLayout(): Int {
        return R.layout.activity_details
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userid = intent.getStringExtra(INTENT_USER_ID)
        userTitle.text = userid

        component.navigationController.navigateToFragment(this, AuthoredFragment.newInstance(userid))

        navigation.setOnNavigationItemSelectedListener { item ->
            lateinit var selectedFragment: Fragment
            when (item.itemId) {
                R.id.authored ->
                    selectedFragment = AuthoredFragment.newInstance(userid)

                R.id.completed ->
                    selectedFragment = CompletedFragment.newInstance(userid)
            }
            component.navigationController.navigateToFragment(this, selectedFragment)
            return@setOnNavigationItemSelectedListener true
        }
    }
}
