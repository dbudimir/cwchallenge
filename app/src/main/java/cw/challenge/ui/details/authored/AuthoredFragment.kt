package cw.challenge.ui.details.authored

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import cw.challenge.R
import cw.challenge.dto.DataItem
import cw.challenge.ui.base.BaseFragment
import cw.challenge.utils.component
import cw.challenge.viewmodel.AuthoredViewModel
import kotlinx.android.synthetic.main.fragment_authored.*

class AuthoredFragment : BaseFragment(), AuthoredAdapter.OnItemClickListener {

    private lateinit var authoredViewModel: AuthoredViewModel
    private lateinit var authoredAdapter: AuthoredAdapter
    private lateinit var userID: String
    companion object {
        private val ARG = "user_id"
        fun newInstance(userid: String): AuthoredFragment {
            val args: Bundle = Bundle()
            args.putSerializable(ARG, userid)
            val fragment = AuthoredFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayout(): Int {
        return R.layout.fragment_authored
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userID = arguments?.getString(ARG).toString()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        authoredViewModel = AuthoredViewModel.create(this)
        component.inject(authoredViewModel)
        authoredViewModel.search(userID)
        progress_bar.visibility = View.VISIBLE
        authoredViewModel.authoredResult.observe(this, Observer {
            progress_bar.visibility = View.INVISIBLE
            authoredAdapter = AuthoredAdapter(it?.data?.data as List<DataItem>, this)
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = authoredAdapter
        })
    }

    override fun onItemClick(item: DataItem) {
        val simpleAlert = AlertDialog.Builder(requireContext()).create()
        simpleAlert.setTitle(item.name)
        simpleAlert.setMessage("Languages: ${item.description}")

        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->
            dialogInterface.dismiss()
        })
        simpleAlert.show()
    }
}
