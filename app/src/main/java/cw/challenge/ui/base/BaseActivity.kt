package cw.challenge.ui.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.Unbinder


/**
 * Created by Dario Budimir on 09/03/18.
 */

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var unbinder: Unbinder

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        unbinder = ButterKnife.bind(this)
    }

    @LayoutRes
    abstract fun getLayout(): Int

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }
}