package cw.challenge.ui.common.search


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import butterknife.OnClick
import cw.challenge.CWBus
import cw.challenge.R
import cw.challenge.dto.UserResponse
import cw.challenge.ui.base.BaseFragment
import cw.challenge.ui.details.DetailsActivity
import cw.challenge.ui.search.SearchAdapter
import cw.challenge.utils.Resource
import cw.challenge.utils.component
import cw.challenge.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * Created by Dario Budimir on 09/03/18.
 */

class SearchFragment : BaseFragment(), SearchAdapter.onItemSelectedListener {

    private lateinit var userViewModel: UserViewModel
    private lateinit var searchAdapter: SearchAdapter
    private val sorted: Boolean = false

    override fun getLayout(): Int {
        return R.layout.fragment_search
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = UserViewModel.create(this)
        component.inject(userViewModel)

        CWBus.listen(String::class.java).subscribe({
            progress_bar.visibility = View.VISIBLE
            userViewModel.search(it)
        })

        userViewModel.searchResult.observe(this, Observer {
            if (it?.status == Resource.Status.SUCCESS && it.data != null){
                Thread {
                    Snackbar.make(recyclerView, R.string.userFound, Snackbar.LENGTH_LONG).show()
                    component.usersDao.insert(it?.data)
                }.start()
            } else {
                Snackbar.make(recyclerView, R.string.noUserFound, Snackbar.LENGTH_LONG).show()
            }
        })

        component.usersDao.getUsers().observe(this, Observer {
            progress_bar.visibility = View.INVISIBLE
            if (it != null) {
                recyclerView.layoutManager = LinearLayoutManager(context)
                searchAdapter = SearchAdapter(it.toList())
                searchAdapter.listener = this
                recyclerView.adapter = searchAdapter
            }
        })
    }

    @OnClick(R.id.filterButton) fun sortResults() {
        searchAdapter.sortItems(!sorted)
    }

    override fun onItemSelected(position: Int, userResponse: UserResponse) {
        val intent = DetailsActivity.newIntent(context, userResponse.username)
        startActivity(intent)
    }
}
