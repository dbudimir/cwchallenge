package cw.challenge.ui.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.view.View
import cw.challenge.R
import cw.challenge.dto.UserResponse
import timber.log.Timber

/**
 * Created by Dario Budimir on 11/03/18.
 */

class SearchAdapter(var userList: List<UserResponse>): RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    lateinit var listener: onItemSelectedListener

    interface onItemSelectedListener {
        fun onItemSelected(position: Int, userResponse: UserResponse)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtName?.text = userList[position].username
        holder.txtTitle?.text = userList[position].name
        holder.itemView.setOnClickListener{
            listener.onItemSelected(position, userList[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.search_item_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return userList.size
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtName = itemView.findViewById<TextView>(R.id.txtName)
        val txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
    }

    fun sortItems(flag: Boolean) {
        if (flag)
            userList = userList.sortedWith(compareBy({ it.leaderboardPosition }))
        else
            userList = userList.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER, { it.username }))
        notifyDataSetChanged()
    }
}