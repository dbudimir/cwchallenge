package cw.challenge

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject



/**
 * Created by Dario Budimir on 10/03/18.
 */

object CWBus {

    private val publisher = PublishSubject.create<Any>()

    fun publish(o: Any) {
        publisher.onNext(o)
    }

    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
}