package cw.challenge.viewmodel

import android.arch.lifecycle.*
import android.support.v4.app.Fragment
import cw.challenge.repository.CWRepository
import javax.inject.Inject


/**
 * Created by Dario Budimir on 11/03/18.
 */

class CompletedViewModel: ViewModel(){
    private lateinit  var repository: CWRepository
    private val searchInput: MutableLiveData<String> = MutableLiveData()
    private val searchPage: MutableLiveData<Int> = MutableLiveData()

    val completedResult = Transformations.switchMap(searchInput) {
        if (it.length >= 3) {
            repository.completedChallenges(it, searchPage.value!!)
        } else {
            MutableLiveData()
        }
    }

    @Inject
    fun init(repository: CWRepository) {
        this.repository = repository
    }

    fun search(term: String, pageNo: Int){
        searchInput.value = term
        searchPage.value = pageNo
    }

    companion object{
        fun create(fragment: Fragment): CompletedViewModel{
            return ViewModelProviders.of(fragment).get(CompletedViewModel::class.java)
        }
    }
}