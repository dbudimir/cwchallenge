package cw.challenge.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations.switchMap
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import cw.challenge.repository.CWRepository
import javax.inject.Inject

/**
 * Created by Dario Budimir on 08/03/18.
 */

class UserViewModel: ViewModel(){
    private lateinit  var repository: CWRepository
    private val searchInput: MutableLiveData<String> = MutableLiveData()

    val searchResult = switchMap(searchInput){
        if(it.length >= 3) {
            repository.searchUser(it)
        } else {
            MutableLiveData()
        }
    }

    val dbResult = switchMap(searchInput){
        if(it.length >= 3) {
            repository.searchUser(it)
        } else {
            MutableLiveData()
        }
    }

    @Inject
    fun init(repository: CWRepository) {
        this.repository = repository
    }

    fun search(term: String){
        searchInput.value = (term)
    }

    companion object{
        fun create(fragment: Fragment): UserViewModel{
            return ViewModelProviders.of(fragment).get(UserViewModel::class.java)
        }
    }
}