package cw.challenge.utils

import android.content.Context
import android.support.v4.app.Fragment
import cw.challenge.CWApplication
import cw.challenge.di.component.AppComponent

/**
 * Created by Dario Budimir on 11/03/18.
 */

val Context.component: AppComponent
    get() = CWApplication.appComponent

val Fragment.component: AppComponent
    get() = activity!!.component

