package cw.challenge.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 08/03/18.
 */

@Module
class AppModule(val application: Application){

    @Provides
    @Singleton
    fun provideApplication() : Application {
        return application
    }
}