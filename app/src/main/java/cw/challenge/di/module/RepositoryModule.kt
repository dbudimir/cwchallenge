package cw.challenge.di.module

import cw.challenge.api.CWApi
import cw.challenge.repository.CWRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 08/03/18.
 */

@Module
class RepositoryModule{
    @Provides
    @Singleton
    fun provideCWRepository(api: CWApi): CWRepository {
        return CWRepository(api)
    }
}