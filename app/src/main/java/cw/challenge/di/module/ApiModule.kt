package cw.challenge.di.module

import cw.challenge.api.CWApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 08/03/18.
 */

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesCWApi(retrofit: Retrofit): CWApi {
        return retrofit.create<CWApi>(CWApi::class.java)
    }
}