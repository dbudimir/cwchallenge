package cw.challenge.di.module

import android.app.Application
import android.arch.persistence.room.Room
import cw.challenge.db.UsersDao
import cw.challenge.db.UsersDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 10/03/18.
 */

@Module
class DaoModule(application: Application) {
    private val DATABASE_NAME = "users-db"

    private val usersDatabase: UsersDatabase = Room.databaseBuilder(application,
            UsersDatabase::class.java,
            DATABASE_NAME)
            .build()

    @Provides
    @Singleton
    fun provideUsersDatabase(): UsersDatabase {
        return usersDatabase
    }

    @Provides
    @Singleton
    fun provideUsersDao(db: UsersDatabase): UsersDao {
        return db.usersDao()
    }
}