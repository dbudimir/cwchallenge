package cw.challenge.di.component

import android.app.Application
import android.content.Context
import cw.challenge.CWApplication
import cw.challenge.db.UsersDao
import cw.challenge.di.module.*
import cw.challenge.repository.CWRepository
import cw.challenge.ui.common.NavigationController
import cw.challenge.viewmodel.AuthoredViewModel
import cw.challenge.viewmodel.CompletedViewModel
import cw.challenge.viewmodel.UserViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Dario Budimir on 08/03/18.
 */

@Singleton
@Component(
        modules = [AppModule::class, NetworkModule::class, RepositoryModule::class, ApiModule::class, DaoModule::class]
)
interface AppComponent {
    val navigationController: NavigationController
    val usersDao: UsersDao

    fun inject(viewModelModule: UserViewModel)
    fun inject(authoredViewModel: AuthoredViewModel)
    fun inject(completedViewModel: CompletedViewModel)
    fun inject(activity: Context)
    fun inject(instance: CWApplication)
    fun provideCWRepository(): CWRepository
    fun provideUsersDao(): UsersDao

    companion object Factory{
        fun create(app: Application, baseUrl: String): AppComponent {
            return DaggerAppComponent.builder().
                    appModule(AppModule(app)).
                    apiModule(ApiModule()).
                    networkModule(NetworkModule(baseUrl)).
                    repositoryModule(RepositoryModule()).
                    daoModule(DaoModule(app)).
                    build()
        }
    }
}