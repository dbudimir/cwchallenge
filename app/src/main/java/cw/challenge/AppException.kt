package cw.challenge

/**
 * Created by Dario Budimir on 08/03/18.
 */

class AppException(val exception: Throwable?): Exception()
