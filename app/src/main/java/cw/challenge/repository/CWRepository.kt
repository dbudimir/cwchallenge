package cw.challenge.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cw.challenge.AppException
import cw.challenge.api.CWApi
import cw.challenge.dto.AuthoredResponse
import cw.challenge.dto.CompletedResponse
import cw.challenge.dto.UserResponse
import cw.challenge.utils.Resource
import javax.inject.Inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
/**
 * Created by Dario Budimir on 08/03/18.
 */

class CWRepository @Inject constructor(val api: CWApi) {

    fun searchUser(userQuery: String): LiveData<Resource<UserResponse>> {
        val data = MutableLiveData<Resource<UserResponse>>()

        api.searchUser(userQuery).enqueue(object : Callback<UserResponse> {
            override fun onResponse(call: Call<UserResponse>?, response: Response<UserResponse>?) {
                data.value = Resource.success(response?.body())
            }

            override fun onFailure(call: Call<UserResponse>?, t: Throwable?) {
                val exception = AppException(t)
                data.value = Resource.error( exception)
            }
        })
        return data
    }

    fun completedChallenges(userQuery: String, pageNo: Int): LiveData<Resource<CompletedResponse>> {
        val data = MutableLiveData<Resource<CompletedResponse>>()

        api.completedChallenges(userQuery, pageNo).enqueue(object : Callback<CompletedResponse> {
            override fun onResponse(call: Call<CompletedResponse>?, response: Response<CompletedResponse>?) {
                data.value = Resource.success(response?.body())
            }

            override fun onFailure(call: Call<CompletedResponse>?, t: Throwable?) {
                val exception = AppException(t)
                data.value = Resource.error( exception)
            }
        })
        return data
    }

    fun authoredChallenges(userQuery: String): LiveData<Resource<AuthoredResponse>> {
        val data = MutableLiveData<Resource<AuthoredResponse>>()

        api.authoredChallenges(userQuery).enqueue(object : Callback<AuthoredResponse> {
            override fun onResponse(call: Call<AuthoredResponse>?, response: Response<AuthoredResponse>?) {
                data.value = Resource.success(response?.body())
            }

            override fun onFailure(call: Call<AuthoredResponse>?, t: Throwable?) {
                val exception = AppException(t)
                data.value = Resource.error( exception)
            }
        })
        return data
    }
}