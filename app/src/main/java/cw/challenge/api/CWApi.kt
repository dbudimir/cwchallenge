package cw.challenge.api

import cw.challenge.dto.AuthoredResponse
import cw.challenge.dto.CompletedResponse
import cw.challenge.dto.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Dario Budimir on 08/03/18.
 */

interface CWApi {
    @GET("users/{id_or_username}")
    fun searchUser(@Path("id_or_username") idOrUsername: String): Call<UserResponse>

    @GET("users/{id_or_username}/code-challenges/completed")
    fun completedChallenges(@Path("id_or_username") idOrUsername: String,
                                     @Query("pageNo") pageNo: Int): Call<CompletedResponse>

    @GET("users/{id_or_username}/code-challenges/authored")
    fun authoredChallenges(@Path("id_or_username") idOrUsername: String): Call<AuthoredResponse>
}